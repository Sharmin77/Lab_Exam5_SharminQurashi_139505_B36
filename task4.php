
<?php
class factorial_of_a_number
{
    protected $_number;
    public function __construct($number)
    {
        if (!is_int($number))
        {
            throw new InvalidArgumentException('Not a number or missing argument');
        }
        $this->_n = $number;
    }
    public function result()
    {
        $factorial = 1;
        for ($i = 1; $i <= $this->_n; $i++)
        {
            $factorial *= $i;
        }
        return $factorial;
    }
}

$newfactorial = New factorial_of_a_number(5);
echo $newfactorial->result();
?>
