<?php

$startDate = new DateTime("1981-11-03 10:34:09");
$endDate = new DateTime(date("D M j G:i:s T Y"));

$interval = $startDate->diff($endDate);
echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days, " .$interval->h." hours, "
    .$interval->i." minute, " .$interval->s." second ";


?>

